#ifndef BSA_BA2_FILE_H
#define BSA_BA2_FILE_H

#include <string>
#include <cstdint>

namespace Bsa
{
    uint32_t generateHash(const std::string& name);
}

#endif
